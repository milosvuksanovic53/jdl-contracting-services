package com.example.jdlcontractingservices;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class GalleryFragment extends Fragment {

    Dialog myDialog;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_gallery, container, false);

        Button gal_1 = view.findViewById(R.id.gallery_img_1);
        Button gal_2 = view.findViewById(R.id.gallery_img_2);
        Button gal_3 = view.findViewById(R.id.gallery_img_3);
        Button gal_4 = view.findViewById(R.id.gallery_img_4);
        Button gal_5 = view.findViewById(R.id.gallery_img_5);
        Button gal_6 = view.findViewById(R.id.gallery_img_6);
        Button gal_7 = view.findViewById(R.id.gallery_img_7);
        Button gal_8 = view.findViewById(R.id.gallery_img_8);
        Button gal_9 = view.findViewById(R.id.gallery_img_9);
        Button gal_10 = view.findViewById(R.id.gallery_img_10);
        Button gal_11 = view.findViewById(R.id.gallery_img_11);
        Button gal_12 = view.findViewById(R.id.gallery_img_12);

        //now we set our onclick listenener..
        gal_1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                ImageDialogOne dialog = new ImageDialogOne();
                dialog.show(getFragmentManager(), "dialog");

            }
        });

        gal_2.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                ImageDialogTwo dialog = new ImageDialogTwo();
                dialog.show(getFragmentManager(), "dialog");

            }
        });

        gal_3.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                ImageDialogFour dialog = new ImageDialogFour();
                dialog.show(getFragmentManager(), "dialog");

            }
        });

        gal_4.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                ImageDialogThree dialog = new ImageDialogThree();
                dialog.show(getFragmentManager(), "dialog");

            }
        });

        gal_5.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                ImageDialogSix dialog = new ImageDialogSix();
                dialog.show(getFragmentManager(), "dialog");

            }
        });

        gal_6.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                ImageDialogFive dialog = new ImageDialogFive();
                dialog.show(getFragmentManager(), "dialog");

            }
        });

        gal_7.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                ImageDialogSeven dialog = new ImageDialogSeven();
                dialog.show(getFragmentManager(), "dialog");

            }
        });

        gal_8.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                ImageDialogEight dialog = new ImageDialogEight();
                dialog.show(getFragmentManager(), "dialog");

            }
        });

        gal_9.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                ImageDialogNine dialog = new ImageDialogNine();
                dialog.show(getFragmentManager(), "dialog");

            }
        });

        gal_10.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                ImageDialogTen dialog = new ImageDialogTen();
                dialog.show(getFragmentManager(), "dialog");

            }
        });

        gal_11.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                ImageDialogEleven dialog = new ImageDialogEleven();
                dialog.show(getFragmentManager(), "dialog");

            }
        });

        gal_12.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                ImageDialogTwelve dialog = new ImageDialogTwelve();
                dialog.show(getFragmentManager(), "dialog");

            }
        });

        return view;
    }

}
