package com.example.jdlcontractingservices;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

public class HomeFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_home,container,false);


        Button goToServices = view.findViewById(R.id.goToServicesBtn);
        Button goToContactUs = view.findViewById(R.id.goToContactUsBtn);
        Button goToAboutUs = view.findViewById(R.id.goToAboutUsBtn);
        Button goToViewServices = view.findViewById(R.id.viewServicesButton);
    //    Button openFacebookWebPage = view.findViewById(R.id.openFacebookAccWebPage);
        Button openInstagramWebPage = view.findViewById(R.id.openInstagramAccWebPage);

        goToServices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentTransaction goToServicesPage = getFragmentManager().beginTransaction();
                goToServicesPage.replace(R.id.fragment_container, new ServicesFragment());
                goToServicesPage.commit();
            }
        });

        goToContactUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentTransaction goToContactUs = getFragmentManager().beginTransaction();
                goToContactUs.replace(R.id.fragment_container, new ContactUsFragment());
                goToContactUs.commit();
            }
        });

        goToAboutUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentTransaction goToAboutUs = getFragmentManager().beginTransaction();
                goToAboutUs.replace(R.id.fragment_container, new AboutUsFragment());
                goToAboutUs.commit();
            }
        });

        goToViewServices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentTransaction goToAboutUs = getFragmentManager().beginTransaction();
                goToAboutUs.replace(R.id.fragment_container, new ServicesFragment());
                goToAboutUs.commit();
            }
        });

        openInstagramWebPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent instagramAccIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.instagram.com/jdlcontractingservices/?igshid=1mf6i1xpzl907&fbclid=IwAR2REMXT8R2w1zmBfJX1rvoRt7afBr-kzuZySWwHqypS1sblCN7annQ8vEc"));
                startActivity(instagramAccIntent);
            }
        });

      //  openFacebookWebPage.setOnClickListener(new View.OnClickListener() {
          //  @Override
           // public void onClick(View v) {
          //      Toast.makeText(getContext(), "Add Facebook Url into a new Intent on this onclick listenner.", Toast.LENGTH_SHORT).show();
          //  }
    //    });

        return view;
    }


}
