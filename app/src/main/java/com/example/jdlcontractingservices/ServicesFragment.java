package com.example.jdlcontractingservices;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatDialog;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class ServicesFragment extends Fragment {

    Dialog myDialog;

    @Nullable
    @Override

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_services,container,false);

        Button new_home_service_button = view.findViewById(R.id.new_homes_btn);
        Button switchboards_button = view.findViewById(R.id.switchboard_btn);
        Button smoke_alarms_button = view.findViewById(R.id.smoke_alarms_btn);
        Button security_alarms_button = view.findViewById(R.id.security_alarms_btn);
        Button lighting_button = view.findViewById(R.id.lighting_btn);
        Button cctv_button = view.findViewById(R.id.cctv_btn);
        Button domestic_electrical_work_button = view.findViewById(R.id.domestic_electrical_work_btn);
        Button commercial_services_button = view.findViewById(R.id.commercial_services_btn);
        Button phone_internet_cabling_button = view.findViewById(R.id.phone_and_internet_cabling_btn);
        Button solar_panel_button = view.findViewById(R.id.solar_panel_btn);
        Button fault_finding_service_button = view.findViewById(R.id.fault_finding_service_btn);

        //when we press the 2nd option on the documentation page it will lead onto
        //the morale page.

        new_home_service_button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                CustomDialog dialog = new CustomDialog();
                dialog.show(getFragmentManager(), "dialog");

            }
        });

        switchboards_button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                SwitchboardsDialog dialog = new SwitchboardsDialog();
                dialog.show(getFragmentManager(), "dialog");

            }
        });

        smoke_alarms_button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                SmokeAlarmsDialog dialog = new SmokeAlarmsDialog();
                dialog.show(getFragmentManager(), "dialog");

            }
        });

        security_alarms_button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                SecurityAlarmsDialog dialog = new SecurityAlarmsDialog();
                dialog.show(getFragmentManager(), "dialog");

            }
        });

        lighting_button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                LightingDialog dialog = new LightingDialog();
                dialog.show(getFragmentManager(), "dialog");

            }
        });

        cctv_button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                CctvDialog dialog = new CctvDialog();
                dialog.show(getFragmentManager(), "dialog");

            }
        });

        domestic_electrical_work_button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                DomesticElectricalWorkDialog dialog = new DomesticElectricalWorkDialog();
                dialog.show(getFragmentManager(), "dialog");

            }
        });

        commercial_services_button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                CommercialServicesDialog dialog = new CommercialServicesDialog();
                dialog.show(getFragmentManager(), "dialog");

            }
        });

        phone_internet_cabling_button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                PhoneInternetCablingDialog dialog = new PhoneInternetCablingDialog();
                dialog.show(getFragmentManager(), "dialog");

            }
        });

        solar_panel_button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                AirConditioningCablingDialog dialog = new AirConditioningCablingDialog();
                dialog.show(getFragmentManager(), "dialog");

            }
        });

        fault_finding_service_button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                FaultFindingServiceDialog dialog = new FaultFindingServiceDialog();
                dialog.show(getFragmentManager(), "dialog");

            }
        });

        return view;
    }

}
