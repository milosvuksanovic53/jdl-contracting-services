package com.example.jdlcontractingservices;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class TwentyFourHourElectricianFragment extends Fragment {
//Here is the Phone number string... you will need to use the Area code so +61........
    //this is just a test number. try it with your own one
    String phoneNumber = "+61419888696";
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_twentyfourhour_electrician, container, false);

        Button twenty_hour_electrician_btn = view.findViewById(R.id.tw_contactUsButton);
        Button twenty_hour_electrician_email_btn = view.findViewById(R.id.email_contactUsButton);

        //when we press the 2nd option on the documentation page it will lead onto
        //the morale page.
        twenty_hour_electrician_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            //this seems to work fine,
                //this is the way to make a call without adding any permissions into the android manifest file.
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phoneNumber,null));
                startActivity(intent);
            }
        });

        twenty_hour_electrician_email_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentTransaction goToContactUsFragment = getFragmentManager().beginTransaction();
                goToContactUsFragment.replace(R.id.fragment_container, new ContactUsFragment());
                goToContactUsFragment.commit();

                //this seems to work fine,
                //this is the way to make a call without adding any permissions into the android manifest file.
                //getFragmentManager().beginTransaction().replace(R.id.nav_contact_us, new ContactUsFragment()).commit();
            }
        });

        return view;

    }
}
