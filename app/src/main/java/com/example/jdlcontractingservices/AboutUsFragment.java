package com.example.jdlcontractingservices;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class AboutUsFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_about_us, container, false);

        //ref our btn from XML file
        Button contactUsbtn = (Button) view.findViewById(R.id.contactUsButton);


        //HomeFragment needs to be added into the AndroidManifest.xml file
        contactUsbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            //replace current fragment with Contact Us Fragment...
                //now goes to contact us fragment.
                FragmentTransaction goToContactUsFragment = getFragmentManager().beginTransaction();
                goToContactUsFragment.replace(R.id.fragment_container, new ContactUsFragment());
                goToContactUsFragment.commit();

            }
        });

        return view;

    }


}
